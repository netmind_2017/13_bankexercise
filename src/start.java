import java.util.Scanner;

public class start {

	public static void main(String[] args) {
		
		Scanner stdin = new Scanner(System.in);

		// Get the starting balance.		
		System.out.println("Welcome to the Bank.");
		System.out.println("What is your starting balance?");
		int balance = stdin.nextInt();
		
		// Print the menu and get the user's choice.
		System.out.println("Here are your menu options:");
		System.out.println("1. Make a deposit.");
		System.out.println("2. Make a withdrawal.");
		System.out.println("3. Print your balance.");
		System.out.println("4. Quit.");
		System.out.println("Please enter the number of your choice.");
		int choice = stdin.nextInt();
		
		// Continue processing choices until the user quits.
		while (choice != 4) {
			
			// Handle a deposit.
			if (choice == 1) {
				System.out.println("Enter the amount of your deposit.");
				int deposit = stdin.nextInt();
				balance = balance + deposit;
			}
			// Handle a withdrawal.
			else if (choice == 2) {
				System.out.println("Enter the amount of your withdrawal.");
				int withdrawal = stdin.nextInt();
				balance = balance - withdrawal;
			}
			// Print out the current balance.
			else if (choice == 3) {
				System.out.println("Your balance is $"+balance+".");
			}
			// Print out an error message for an invalid choice.
			else if (choice != 4) {
				System.out.println("Sorry that is not a valid choice.");
			}
			
			// Reprompt the menu.
			System.out.println("Here are your menu options:");
			System.out.println("1. Make a deposit.");
			System.out.println("2. Make a withdrawal.");
			System.out.println("3. Print your balance.");
			System.out.println("4. Quit.");
			System.out.println("Please enter the number of your choice.");
			choice = stdin.nextInt();
		}
		
		System.out.println("Thank you for using the bank!");
		
		stdin.close();
	}	
}
